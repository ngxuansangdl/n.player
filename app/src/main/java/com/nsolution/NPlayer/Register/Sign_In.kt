package com.nsolution.NPlayer.Register

import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nsolution.NPlayer.Base.BaseFragment
import com.nsolution.NPlayer.Get_Data_API.HttpClient
import com.nsolution.NPlayer.Home_Screen.Homescreen
import com.nsolution.NPlayer.Interface_Service.AccountResult
import com.nsolution.NPlayer.Interface_Service.IAccountServiceAPI
import com.nsolution.NPlayer.Interface_Service.Result
import com.nsolution.NPlayer.Interface_Service.SignInInputDto
import com.nsolution.NPlayer.Navigation
import com.nsolution.NPlayer.PrefData
import com.nsolution.NPlayer.R
import com.nsolution.NPlayer.Register.Forgot_Password.Forgot_Password
import kotlinx.android.synthetic.main.fragment_sign_in.*
import retrofit2.Call
import retrofit2.Response


class Sign_In : BaseFragment() {
  var email: String = ""
  var password: String = ""
  val SUCCESS: Int = 200
  val INTERNAL_SERVER_ERROR: Int = 500
//  val loadingAlerts = Loading(requireActivity().parent)
  override fun provideYourFragmentView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View {
    return inflater.inflate(R.layout.fragment_sign_in, container, false)
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    onClick()
    onChangeText(enterEmail, errorEmail)
    onChangeText(enterPassword, errorPassword)
  }

  private fun onClick() {
    signInButton.setOnClickListener {
      email = enterEmail.text.toString()
      password = enterPassword.text.toString()
      if (email.isEmpty())
        errorEmail.text = "Please enter email"
      if (password.isEmpty())
        errorPassword.text = "Please enter password"
      if ((email.isNotEmpty() && password.isNotEmpty()))
      {
        //loadingAlerts.startLoadingAlert()
        SignIn(SignInInputDto(email,password,true)).onfetch()
      }
    }
    forgot_password_click.setOnClickListener{
      replaceFragment(Forgot_Password())
    }
  }
  private fun SignIn(input : SignInInputDto) =
    object : HttpClient<Result<AccountResult>>() {
      override fun callSync(): Call<Result<AccountResult>> {
        val service: IAccountServiceAPI = retrofit().create(IAccountServiceAPI::class.java)
        return service.signIn(input)
      }
      override fun handling(response: Response<Result<AccountResult>>) {
        //loadingAlerts.closeLoadingAlert()
        if(response.isSuccessful){
          val authToken = response.body()!!.result!!.authToken
          PrefAuthToken().setPrefData(authToken!!)
          Navigation(requireActivity()).gotoActivity(Homescreen::class.java,null,null,true)
        } else ToastMessenger("Login fail")
      }
    }
  private fun PrefAuthToken() = object : PrefData<String>(context!!, "Authorization") {
    override fun getPrefData(): String? {
      return null
    }
    override fun setPrefData(data: String?) {
      editor.putString("haveAuthToken",data).commit()
    }
  }
}
