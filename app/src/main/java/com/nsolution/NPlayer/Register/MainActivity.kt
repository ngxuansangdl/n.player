package com.nsolution.NPlayer.Register

import android.os.Bundle
import com.nsolution.NPlayer.Base.BaseActivity
import com.nsolution.NPlayer.R
import kotlinx.android.synthetic.main.activity_main.*

open class MainActivity : BaseActivity() {

  override fun getLayoutID(): Int {
    return R.layout.activity_main
  }

  override fun onCreateActivity(savedInstanceState: Bundle?) {
    replaceFragment(R.id.container,Menu_Register())
  }
}
