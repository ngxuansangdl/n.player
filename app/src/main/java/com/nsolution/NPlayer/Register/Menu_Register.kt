package com.nsolution.NPlayer.Register

import android.R.attr.data
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.nsolution.NPlayer.Base.BaseFragment
import com.nsolution.NPlayer.Get_Data_API.HttpClient
import com.nsolution.NPlayer.Interface_Service.AccountResult
import com.nsolution.NPlayer.Interface_Service.IAccountServiceAPI
import com.nsolution.NPlayer.Interface_Service.SignInWithFacebookDto
import com.nsolution.NPlayer.Interface_Service.SignInWithGoogleDto
import com.nsolution.NPlayer.R
import com.nsolution.NPlayer.Register.Sign_Up.Sign_Up
import kotlinx.android.synthetic.main.fragment_menu_register.*
import retrofit2.Call
import retrofit2.Response


class Menu_Register : BaseFragment() {

  override fun provideYourFragmentView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View {
    return inflater.inflate(R.layout.fragment_menu_register, container, false)
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    onClick()
  }
  private fun onClick(){
    signin.setOnClickListener {
      replaceFragment(Sign_In())
    }
    signup.setOnClickListener {
      replaceFragment(Sign_Up())
    }
    facebook.setOnClickListener {

    }
    google.setOnClickListener {
//      try {
//        val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
//        val idToken = task.getResult(ApiException::class.java)!!.idToken
//        if(idToken != null)
//
//      } catch (e : Exception){
//
//      }
    }
  }
//  private fun signInWithFacebookOrGoogle(input : Any) =
//    object : HttpClient<AccountResult>() {
//      override fun callSync(): Call<AccountResult> {
//        val service: IAccountServiceAPI = retrofit().create(IAccountServiceAPI::class.java)
////        return service.forgotPassword(ForgotInputDto(email))
//        if(input is SignInWithGoogleDto){
//          return service.signInWithGoogle(SignInWithGoogleDto())
//        }
//        return service.signInWithFacebook(SignInWithFacebookDto())
//      }
//      override fun handling(response: Response<AccountResult>) {
//        Log.d("result",response.toString())
//
//        if(response.isSuccessful){
//
//        }
//      }
//    }
}
