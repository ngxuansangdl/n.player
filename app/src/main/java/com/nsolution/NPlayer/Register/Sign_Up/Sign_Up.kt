package com.nsolution.NPlayer.Register.Sign_Up

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nsolution.NPlayer.Base.BaseFragment
import com.nsolution.NPlayer.R
import kotlinx.android.synthetic.main.fragment_sign_up.*

class Sign_Up : BaseFragment() {

    private var email = ""
    private var password = ""
    private var confirmPassword = ""

    override fun provideYourFragmentView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_sign_up, container, false)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onClick()
        onChangeText(enterEmail,errorEmail)
    }
    private fun onClick(){
        nextButton.setOnClickListener {
          replaceFragment(Add_Infomation())
        }
    }
}
