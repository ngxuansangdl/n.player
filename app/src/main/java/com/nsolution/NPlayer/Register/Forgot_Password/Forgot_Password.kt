package com.nsolution.NPlayer.Register.Forgot_Password

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nsolution.NPlayer.Base.BaseFragment
import com.nsolution.NPlayer.R
import kotlinx.android.synthetic.main.fragment_forgot_password.*
import kotlinx.android.synthetic.main.fragment_forgot_password.enterEmail
import kotlinx.android.synthetic.main.fragment_forgot_password.errorEmail

private const val EMAIL = "Email"

class Forgot_Password : BaseFragment()  {
    override fun provideYourFragmentView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_forgot_password, container, false)
    }
    var email: String = ""
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sendButton.setOnClickListener {
            email = enterEmail.text.toString()
            if (email.isEmpty())
                errorEmail.text = "Please enter email"
            else {
                val verifyCode = Verify_Code()
                val bundle = Bundle()
                bundle.putString(EMAIL, email)
                verifyCode.arguments = bundle
                replaceFragment(verifyCode)
            }
        }
    }
    companion object {
        @JvmStatic
        fun newInstance(email: String) =
            Forgot_Password().apply {
                arguments = Bundle().apply {
                    putString(EMAIL, email)
                }
            }
    }
}
