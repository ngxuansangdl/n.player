package com.nsolution.NPlayer.Register.Forgot_Password

import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.goodiebag.pinview.Pinview
import com.nsolution.NPlayer.Base.BaseFragment
import com.nsolution.NPlayer.Get_Data_API.HttpClient
import com.nsolution.NPlayer.Interface_Service.ForgotInputDto
import com.nsolution.NPlayer.R
import kotlinx.android.synthetic.main.fragment_verify_code.*
import retrofit2.Call
import retrofit2.Response
import com.nsolution.NPlayer.Interface_Service.AccountResult
import com.nsolution.NPlayer.Interface_Service.IAccountServiceAPI
import com.nsolution.NPlayer.Interface_Service.Result
import com.nsolution.NPlayer.Interface_Service.SignInInputDto
import java.util.*
import kotlin.concurrent.schedule


private const val EMAIL = "Email"
class Verify_Code : BaseFragment() {
  private var email: String? = null
  private var resendTimeDuration: Int = 31
  private var resendTimeCurrent: Int = 0
  private var verifyCode : String? = null
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    val bundle : Bundle? = this.arguments
    if(bundle != null){
      email = bundle.getString(EMAIL)
      result(ForgotInputDto(email!!)).onfetch()
    }
  }

  override fun provideYourFragmentView(
    inflater: LayoutInflater, container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View {
    return inflater.inflate(R.layout.fragment_verify_code, container, false)
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    text.text = "We will send a vertification code to ${email}."
    setResendTime()
    pin.setPinViewEventListener(object : Pinview.PinViewEventListener {
      override fun onDataEntered(pinview: Pinview?, fromUser: Boolean) {
        verifyCode = pinview!!.value
      }
    })
    resend_click.setOnClickListener {
      result(ForgotInputDto(email!!)).onfetch()
      setResendTime()
    }
    submit_button.setOnClickListener {

    }

  }
  private fun setResendTime(){
    resend_click.visibility = View.GONE
    resend_time.visibility = View.VISIBLE
    val timer = object: CountDownTimer((resendTimeDuration * 1000).toLong(), 1000) {
      override fun onTick(millisUntilFinished: Long) {
        if(millisUntilFinished > 10000)
          resend_time.text = "0:${millisUntilFinished/1000}"
        else resend_time.text = "0:0${millisUntilFinished/1000}"
      }
      override fun onFinish() {
        resend_click.visibility = View.VISIBLE
        resend_time.visibility = View.GONE
      }
    }
    timer.start()
  }
  private fun result(input: ForgotInputDto) =
    object : HttpClient<Result<AccountResult>>() {
      override fun callSync(): Call<Result<AccountResult>> {
        val service: IAccountServiceAPI = retrofit().create(IAccountServiceAPI::class.java)
        return service.forgotPassword(input)
      }

      override fun handling(response: Response<Result<AccountResult>>) {
        Log.d("result",response.toString())
        if(response.isSuccessful){
          resendTimeDuration = response.body()!!.result!!.resendTime!!
          resendTimeCurrent = resendTimeDuration
        }
      }
    }
  companion object {
    @JvmStatic
    fun newInstance(email: String) =
      Verify_Code().apply {
        arguments = Bundle().apply {
          putString(EMAIL, email)
        }
      }
  }
}
