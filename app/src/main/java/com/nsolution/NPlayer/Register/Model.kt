package com.nsolution.NPlayer.Register

data class SignInDto(
    val email : String,
    val Password : String,
    val IsPersistent : Boolean
)

data class SignInResult(
    val AuthToken : String
)

data class SignUpDto(
    val email : String,
    val Password : String,
    val Phone : String,
    val FullName : String
)

data class SignUpResult(
    val AuthToken : String
)

data class GoogleSignInDto(
    val TokenID : String,
    val AccessToken : String
)

data class UpdatePasswordDto(
    val Password : String,
    val NewPassword : String,
    val ConfirmNewPassword : String
)