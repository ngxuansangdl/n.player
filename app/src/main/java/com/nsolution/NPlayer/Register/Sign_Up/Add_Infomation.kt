package com.nsolution.NPlayer.Register.Sign_Up

import android.graphics.Rect
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.widget.ScrollView
import com.nsolution.NPlayer.Base.BaseFragment
import com.nsolution.NPlayer.Home_Screen.Homescreen
import com.nsolution.NPlayer.Navigation
import com.nsolution.NPlayer.R
import kotlinx.android.synthetic.main.fragment_add_infomation.*


class Add_Infomation : BaseFragment() {
  override fun provideYourFragmentView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View {
    return inflater.inflate(R.layout.fragment_add_infomation, container, false)
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    onClick()

  }

  private fun onClick() {
    finishButton.setOnClickListener {
      Navigation(requireActivity()).gotoActivity(Homescreen::class.java, null, null,true)
    }
  }


}
