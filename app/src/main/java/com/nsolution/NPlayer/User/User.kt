package com.nsolution.NPlayer.User

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.nsolution.NPlayer.Base.BaseActivity
import com.nsolution.NPlayer.R
import kotlinx.android.synthetic.main.activity_user.*


class User : BaseActivity() {

  private val NAME = "NAME"
  private val AVATAR = "AVATAR"
  private val ARTIST = "ARTIST"
  private val IMAGE = "IMAGE"
  private val DefaultPosition = 0

  override fun getLayoutID(): Int {
    return R.layout.activity_user
  }


  override fun onCreateActivity(savedInstanceState: Bundle?) {
    toolBar(toolbar)
    val bundle = intent.extras
    val avatar = bundle?.getString(IMAGE)
    name.text = bundle?.getString(ARTIST)
    setImage(avatar_click,avatar.toString())

    val listFragment : ArrayList<Fragment> = arrayListOf()
    listFragment.add(Post_User())
    listFragment.add(Detail_User())
    setViewPaper(viewPager,tab_layout,listFragment,DefaultPosition)
  }
}
