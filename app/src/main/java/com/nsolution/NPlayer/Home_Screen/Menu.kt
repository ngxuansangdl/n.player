package com.nsolution.NPlayer.Home_Screen

import android.content.Intent
import android.os.Bundle
import com.nsolution.NPlayer.Base.BaseActivity
import com.nsolution.NPlayer.Navigation
import com.nsolution.NPlayer.PrefData
import com.nsolution.NPlayer.Profile.My_Profile
import com.nsolution.NPlayer.R
import com.nsolution.NPlayer.Register.MainActivity
import kotlinx.android.synthetic.main.activity_menu.*


class Menu : BaseActivity() {
  override fun getLayoutID() = R.layout.activity_menu
  override fun onCreateActivity(savedInstanceState: Bundle?) {
    detail_click.setOnClickListener {
      Navigation(this).gotoActivity(My_Profile::class.java,null,null,false)
    }
    sign_out.setOnClickListener {
      val PrefAuthToken = PrefData<String>(applicationContext, "Authorization")
      PrefAuthToken.removePrefData("Authorization")
      val flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
      Navigation(this).gotoActivity(MainActivity::class.java, null,flags, true)
    }
  }

}
