package com.nsolution.NPlayer.Home_Screen

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.nsolution.NPlayer.Adapter.RecyclerView.List_Category
import com.nsolution.NPlayer.Get_Data_API.HttpClient
import com.nsolution.NPlayer.Interface_Service.*

import com.nsolution.NPlayer.R
import kotlinx.android.synthetic.main.fragment_discover.*
import retrofit2.Call
import retrofit2.Response

class Discover : Fragment() {


  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

  }

  override fun onCreateView(
    inflater: LayoutInflater, container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    return inflater.inflate(R.layout.fragment_discover, container, false)
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    //discover().onfetch()

  }
//  private fun discover() =
//    object : HttpClient<Result<ListCategory>>() {
//      override fun callSync(): Call<Result<ListCategory>> {
//        val service: IList = retrofit1().create(IList::class.java)
//        return service.Discover()
//      }
//      override fun handling(response: Response<Result<ListCategory>>) {
//        //loadingAlerts.closeLoadingAlert()
//        if(response.isSuccessful){
//          val result = response.body()!!.result!!
//          val listCategory = result!!.listCategoty
//          parent.layoutManager = LinearLayoutManager(context)
//          parent.adapter = List_Category(listCategory!!)
//        }
//      }
//    }
}
