package com.nsolution.NPlayer.Home_Screen

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nsolution.NPlayer.Adapter.RecyclerView.List_Category
import com.nsolution.NPlayer.Data_Class.layout
import com.nsolution.NPlayer.Get_Data_API.HttpClient
import com.nsolution.NPlayer.Interface_Service.Category
import com.nsolution.NPlayer.Interface_Service.IList
import com.nsolution.NPlayer.Interface_Service.ListCategory
import com.nsolution.NPlayer.Interface_Service.Result
import com.nsolution.NPlayer.Loading
import com.nsolution.NPlayer.R
import kotlinx.android.synthetic.main.fragment_home.*
import retrofit2.Call
import retrofit2.Response


class Home : Fragment() {

  val layout = layout()
  var list : ArrayList<Category> = arrayListOf()
  override fun onCreateView(
    inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
  ): View? {
    return inflater.inflate(R.layout.fragment_home, container, false)
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    homescreen().onfetch()
  }
  private fun homescreen() =
    object : HttpClient<Result<ListCategory>>() {
      override fun callSync(): Call<Result<ListCategory>> {
        val service: IList = retrofit1().create(IList::class.java)
        return service.Homescreen()
      }
      override fun handling(response: Response<Result<ListCategory>>) {
        if(response.isSuccessful){
          progressBar.visibility = View.GONE
          val result = response.body()!!.result!!
          list = result.listCategoty!!
          parent.layoutManager = LinearLayoutManager(context)
          parent.adapter = List_Category(list!!)
        }
      }
    }
  }





