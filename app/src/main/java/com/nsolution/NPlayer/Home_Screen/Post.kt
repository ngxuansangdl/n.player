package com.nsolution.NPlayer.Home_Screen

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nsolution.NPlayer.Base.BaseActivity
import com.nsolution.NPlayer.Navigation
import com.nsolution.NPlayer.Post.Start_LiveStream

import com.nsolution.NPlayer.R
import kotlinx.android.synthetic.main.activity_post.*

class Post : BaseActivity() {
  override fun getLayoutID(): Int = R.layout.activity_post
  override fun onCreateActivity(savedInstanceState: Bundle?) {
    toolBar(toolbar)
    dashboard_live.setOnClickListener {
      Navigation(this).gotoActivity(Start_LiveStream::class.java,null,null,false)
    }
    dashboard_record.setOnClickListener {

    }
    dashboard_storage.setOnClickListener {

    }

  }

}
