package com.nsolution.NPlayer.Home_Screen

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import com.nsolution.NPlayer.Base.BaseActivity
import com.nsolution.NPlayer.Media.Mp3.Mp3
import com.nsolution.NPlayer.Navigation
import com.nsolution.NPlayer.R
import com.nsolution.NPlayer.Search.Search
import kotlinx.android.synthetic.main.activity_homescreen.*


class Homescreen : BaseActivity() {
  override fun onBackPressed() {
    super.onBackPressed()
    supportFragmentManager.popBackStack()
  }

  override fun getLayoutID() = R.layout.activity_homescreen

  override fun onCreateActivity(savedInstanceState: Bundle?) {
    replaceFragment(R.id.container, Home())
    nav_view.selectedItemId = R.id.home
    search_click.setOnClickListener {
      Navigation(this).gotoActivity(Search::class.java, null, null, false)
    }

    avatar_click.setOnClickListener {
      Navigation(this).gotoActivity(Menu::class.java, null, null, false)
    }
    nav_view.setOnNavigationItemSelectedListener {
      when (it.itemId) {
        R.id.home -> {
          replaceFragment(R.id.container, Home())
          header.visibility = View.VISIBLE
          return@setOnNavigationItemSelectedListener true
        }
        R.id.discover -> {
          replaceFragment(R.id.container, Discover())
          header.visibility = View.VISIBLE
          return@setOnNavigationItemSelectedListener true
        }
        R.id.post -> {
          Navigation(this).gotoActivity(Post::class.java, null, null, false)
        }
        R.id.notification -> {
          header.visibility = View.VISIBLE
          replaceFragment(R.id.container, Notification())
          return@setOnNavigationItemSelectedListener true
        }
        R.id.library -> {
          header.visibility = View.VISIBLE
          replaceFragment(R.id.container, Library())
          return@setOnNavigationItemSelectedListener true
        }
      }
      false
    }


  }
}
