package com.nsolution.NPlayer.Get_Data_API

import android.util.Log
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


abstract class HttpClient<T> {
  abstract fun handling(response: Response<T>)
  abstract fun callSync(): Call<T>
  fun retrofit(): Retrofit {
    val httpClient: OkHttpClient.Builder = OkHttpClient.Builder()
    val retrofit: Retrofit = Retrofit.Builder()
      .baseUrl("http://demo-mobile-api.vastbit.com/api/")
      .addConverterFactory(GsonConverterFactory.create())
      .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
      .client(httpClient.build())
      .build()
    return retrofit
  }
  fun retrofit1(): Retrofit {
    val httpClient: OkHttpClient.Builder = OkHttpClient.Builder()
    val retrofit: Retrofit = Retrofit.Builder()
      .baseUrl("http://192.168.1.103:3000/")
      .addConverterFactory(GsonConverterFactory.create())
      .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
      .client(httpClient.build())
      .build()
    return retrofit
  }
  fun onfetch() {
    callSync().enqueue(object : Callback<T> {
      override fun onFailure(call: Call<T>, t: Throwable) {
        t.printStackTrace()
      }
      override fun onResponse(call: Call<T>, response: Response<T>) {
        handling(response)
      }
    })
  }
}

