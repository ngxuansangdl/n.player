package com.nsolution.NPlayer.Interface_Service

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import retrofit2.Call
import retrofit2.http.*
import retrofit2.http.Headers as Headers1


class AccountResult {
  @SerializedName("authToken")
  @Expose
  var authToken: String? = null
  @SerializedName("message")
  @Expose
  var message: String? = null
  @SerializedName("resendTime")
  @Expose
  var resendTime: Int? = null
}

class SignInInputDto(val email: String, val Password: String, val IsPersistent: Boolean)

class ForgotInputDto(val email: String)

class VertifyDto(val Code: String)

class SignInWithGoogleDto(val TokenId: String,val AccessToken : String)

class SignInWithFacebookDto(val AccessToken: String)



interface IAccountServiceAPI {
  @POST("Account/SignIn")
  fun signIn(@Body input: SignInInputDto): Call<Result<AccountResult>>

  @POST("Account/SignIn")
  fun signInWithToken(@Header("Authorization") authToken: String): Call<Result<AccountResult>>

  @POST("Account/ResetPassword/Email")
  fun forgotPassword(@Body input: ForgotInputDto): Call<Result<AccountResult>>

  @POST("Account/SignIn/Google")
  fun signInWithFacebook(@Body input: ForgotInputDto): Call<Result<AccountResult>>

  @POST("Account/SignIn/Facebook")
  fun signInWithGoogle(@Body input: ForgotInputDto): Call<Result<AccountResult>>
}
