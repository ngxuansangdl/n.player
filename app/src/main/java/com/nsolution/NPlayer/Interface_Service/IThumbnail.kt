package com.nsolution.NPlayer.Interface_Service

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.nsolution.NPlayer.Data_Class.Constant
import com.nsolution.NPlayer.Models.*
import retrofit2.Call
import retrofit2.http.GET



class Category {
  @SerializedName("title")
  @Expose
  var title : String? = null
  @SerializedName("type")
  @Expose
  var type : String? = null

  @SerializedName("listSong")
  @Expose
  var listSong : ArrayList<SongDto>? = null

  @SerializedName("listVideo")
  @Expose
  var listVideo : ArrayList<VideoDto>? = null

  @SerializedName("listPlaylist")
  @Expose
  var listPlaylist: ArrayList<PlaylistDto>? = null

  @SerializedName("listArtist")
  @Expose
  var listArtist : ArrayList<ArtistDto>? = null

  @SerializedName("listComment")
  @Expose
  var listComment : ArrayList<CommentDto>? = null

  @SerializedName("listNotification")
  @Expose
  var listNotification : ArrayList<NotificationDto>? = null

  @SerializedName("listTheme")
  @Expose
  var listTheme: ArrayList<ThemeDto>? = null

  @SerializedName("listKeyword")
  @Expose
  var listKeyword: ArrayList<KeywordDto>? = null

}



class ListCategory {
  @SerializedName("listCategoty")
  @Expose
  var listCategoty : ArrayList<Category>? = null
}

interface IList {
  @GET("homescreen")
  fun Homescreen(): Call<Result<ListCategory>>
  @GET("discover")
  fun Discover(): Call<Result<ListCategory>>
  @GET("Notification")
  fun Notification(): Call<Result<Category>>
}
