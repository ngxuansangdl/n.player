package com.nsolution.NPlayer.Interface_Service

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.nsolution.NPlayer.Interface_Service.Result

class Result<T> {
  @SerializedName("result")
  @Expose
  var result: T? = null
  @SerializedName("statusCode")
  @Expose
  var statusCode: Int? = null
}