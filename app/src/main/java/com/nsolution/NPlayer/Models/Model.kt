package com.nsolution.NPlayer.Models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.nsolution.NPlayer.R





class SongDto {
  @SerializedName("title")
  @Expose
  var title: String? = null
  @SerializedName("artists")
  @Expose
  var artists: String? = null
  @SerializedName("image")
  @Expose
  var image: String? = null
  @SerializedName("page")
  @Expose
  var page: String? = null
  @SerializedName("hasNextPage")
  @Expose
  var hasNextPage: String? = null
}

class VideoDto {
  @SerializedName("title")
  @Expose
  var title: String? = null
  @SerializedName("artists")
  @Expose
  var artists: String? = null
  @SerializedName("image")
  @Expose
  var image: String? = null
  @SerializedName("live")
  @Expose
  var live: Boolean? = null
  @SerializedName("page")
  @Expose
  var page: String? = null
  @SerializedName("hasNextPage")
  @Expose
  var hasNextPage: String? = null
}

class ArtistDto{
  @SerializedName("artists")
  @Expose
  val artists: String? = null
  @SerializedName("avatar")
  @Expose
  val avatar: String? = null
  @SerializedName("page")
  @Expose
  val page: String? = null
}

class PlaylistDto {
  @SerializedName("title")
  @Expose
  var title: String? = null
  @SerializedName("artists")
  @Expose
  var artists: String? = null
  @SerializedName("image")
  @Expose
  var image: String? = null
  @SerializedName("page")
  @Expose
  var page: String? = null
  @SerializedName("hasNextPage")
  @Expose
  var hasNextPage: String? = null
}

class CommentDto {
  @SerializedName("name")
  @Expose
  var name: String? = null
  @SerializedName("avatar")
  @Expose
  var avatar: String? = null
  @SerializedName("time")
  @Expose
  var time: String? = null
  @SerializedName("context")
  @Expose
  var context: String? = null
  @SerializedName("page")
  @Expose
  var page: String? = null
  @SerializedName("hasNextPage")
  @Expose
  var hasNextPage: String? = null
}

class NotificationDto {
  @SerializedName("artists")
  @Expose
  var artists: String? = null
  @SerializedName("image")
  @Expose
  var image: String? = null
  @SerializedName("context")
  @Expose
  var context: String? = null
  @SerializedName("page")
  @Expose
  var page: String? = null
  @SerializedName("hasNextPage")
  @Expose
  var hasNextPage: String? = null
}

class ThemeDto {
  @SerializedName("title")
  @Expose
  var title: String? = null
  @SerializedName("image")
  @Expose
  var image: String? = null
  @SerializedName("page")
  @Expose
  var page: String? = null
  @SerializedName("hasNextPage")
  @Expose
  var hasNextPage: String? = null
}

class KeywordDto {
  @SerializedName("keyword")
  @Expose
  var keyword: String? = null

}

