package com.nsolution.NPlayer.Search

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.nsolution.NPlayer.Base.BaseActivity
import com.nsolution.NPlayer.Media.Mp4.Detail_Video
import com.nsolution.NPlayer.Media.Mp4.Next_Video
import com.nsolution.NPlayer.R
import kotlinx.android.synthetic.main.activity_qr.*

class QR_Screen : BaseActivity() {

  override fun getLayoutID(): Int {
    return R.layout.activity_qr
  }
  private val DefaultPosition = 0
  override fun onCreateActivity(savedInstanceState: Bundle?) {
    toolBar(toolbar)
    val listFragment : ArrayList<Fragment> = arrayListOf()
    listFragment.add(My_QR_Code())
    listFragment.add(ScannerQR())
    setViewPaper(viewPager,tab_layout,listFragment,DefaultPosition)
  }
}
