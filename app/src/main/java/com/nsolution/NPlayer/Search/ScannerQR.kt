package com.nsolution.NPlayer.Search

import android.os.Bundle
import android.util.SparseArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.android.gms.vision.barcode.Barcode
import info.androidhive.barcode.BarcodeReader
import info.androidhive.barcode.BarcodeReader.BarcodeReaderListener




class ScannerQR : Fragment(), BarcodeReaderListener {
  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
    return super.onCreateView(inflater, container, savedInstanceState)
  }

  override fun onScanned(barcode: Barcode) { // single barcode scanned
  }

  override fun onScannedMultiple(list: List<Barcode>) { // multiple barcodes scanned
  }

  override fun onBitmapScanned(sparseArray: SparseArray<Barcode>) { // barcode scanned from bitmap image
  }

  override fun onScanError(s: String) { // scan error
  }
}