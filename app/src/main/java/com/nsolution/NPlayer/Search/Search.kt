package com.nsolution.NPlayer.Search

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nsolution.NPlayer.R
import com.nsolution.NPlayer.Adapter.RecyclerView.List_Category
import com.nsolution.NPlayer.Navigation
import kotlinx.android.synthetic.main.activity_search.*

class Search : AppCompatActivity() {
  override fun onBackPressed() {
    super.onBackPressed()
    finish()
  }
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_search)
    val contextList : ArrayList<Any> = arrayListOf()
    val history : ArrayList<Any> = arrayListOf()
    search_bar.queryHint = "Singer, song, or podcast"



    qr_click.setOnClickListener {
      Navigation(this).gotoActivity(QR_Screen::class.java,null,null,false)
    }


    val theme : RecyclerView = findViewById<RecyclerView>(R.id.themeList)

  }
}
