package com.nsolution.NPlayer

import android.app.Notification
import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.media.AudioManager
import android.media.MediaPlayer
import android.os.Binder
import android.os.IBinder
import android.os.PowerManager
import android.util.Log
import com.nsolution.NPlayer.Home_Screen.Homescreen
import com.nsolution.NPlayer.Models.SongDto


class MediaService : Service(),
  MediaPlayer.OnPreparedListener,
  MediaPlayer.OnErrorListener,
  MediaPlayer.OnCompletionListener
{

  private var mediaPlayer : MediaPlayer? = null
  private var songPosition : Int = 0
  private var uri : String? = null
  private var title : String? = null
  private var listSong : ArrayList<SongDto>? = null

  private val musicBind: IBinder = MusicBinder()


  open var setList : ArrayList<SongDto>? = null
  open var setUri : String? = null
  open var setSongPosition : Int = 0
  class MusicBinder : Binder() {
    fun getService() : MediaService = MediaService()
  }


  override fun onCreate() {
    super.onCreate()
    mediaPlayer = MediaPlayer()
    initMusicPlayer()
    playSong()
  }


  fun initMusicPlayer(){
    mediaPlayer!!.apply {
      setWakeMode(applicationContext,PowerManager.PARTIAL_WAKE_LOCK)
      setAudioStreamType(AudioManager.STREAM_MUSIC)
      setOnPreparedListener(this@MediaService)
      setOnErrorListener(this@MediaService)
      setOnCompletionListener(this@MediaService)
    }

  }

  fun playSong() {
    Log.d("listSong", setList.toString())
//    mediaPlayer!!.reset()
    if(setList != null && setUri != null){
      listSong = setList
      var itemSong = listSong!!.get(setSongPosition)
      title = itemSong!!.title
      try {
        mediaPlayer!!.setDataSource(uri)
        mediaPlayer!!.prepare()
      } catch (e: Exception) {

      }
      mediaPlayer!!.prepareAsync()
    }
  }
  override fun onBind(intent: Intent): IBinder {
    return musicBind
  }

  override fun onPrepared(mp: MediaPlayer?) {
    mediaPlayer!!.start()
    val notificationIntent = Intent(this,Homescreen::class.java)
    notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
    val pendingIntent = PendingIntent.getActivity(this,0,notificationIntent,PendingIntent.FLAG_UPDATE_CURRENT)
    val notificationBuilder : Notification.Builder = Notification.Builder(this)
    notificationBuilder
      .setContentIntent(pendingIntent)
      .setSmallIcon(R.drawable.icon_nplayer)
      .setOngoing(true)
      .setContentTitle("Playing")
      .setContentText(title)
    val notification : Notification = notificationBuilder.notification
    startForeground(1,notification)
  }

  override fun onError(mp: MediaPlayer?, what: Int, extra: Int): Boolean {
    mp!!.reset()
    return false
  }

  override fun onCompletion(mp: MediaPlayer?) {
    if(mediaPlayer!!.currentPosition > 0){
      mp!!.reset()

    }
  }

  fun nextSong(){
    songPosition++
    if(songPosition >= listSong!!.size)
      songPosition = 0
    playSong()
  }
  fun previousSong() {
    songPosition--
    if (songPosition < 0) songPosition = listSong!!.size - 1
    playSong()
  }
  fun getPosn(): Int {
    return mediaPlayer!!.getCurrentPosition()
  }

  fun getDur(): Int {
    return mediaPlayer!!.getDuration()
  }

  fun isPng(): Boolean {
    return mediaPlayer!!.isPlaying()
  }

  fun pausePlayer() {
    mediaPlayer!!.pause()
  }

  fun seek(posn: Int) {
    mediaPlayer!!.seekTo(posn)
  }

  fun go() {
    mediaPlayer!!.start()
  }
  override fun onUnbind(intent: Intent?): Boolean {
    return super.onUnbind(intent)
    mediaPlayer!!.stop()
    mediaPlayer!!.release()
    return false
  }

  override fun onDestroy() {
    super.onDestroy()
    stopForeground(true)
  }
}
