package com.nsolution.NPlayer

import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater

class Loading(context: Context){
  val alertDialog: AlertDialog = AlertDialog.Builder(context).create()
  val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

  fun startLoadingAlert(){
    val view = inflater.inflate(R.layout.layout_loading,null)
    alertDialog.setView(view)
    alertDialog.show()
  }
  fun closeLoadingAlert(){
    alertDialog.dismiss()
  }
}