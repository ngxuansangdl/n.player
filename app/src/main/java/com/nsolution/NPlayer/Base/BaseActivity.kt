package com.nsolution.NPlayer.Base

import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.os.Handler
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.nsolution.NPlayer.Adapter.adapterTabLayout
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_user.tab_layout
import kotlinx.android.synthetic.main.activity_user.viewPager


abstract class BaseActivity : AppCompatActivity() {
  abstract fun getLayoutID(): Int
  abstract fun onCreateActivity(savedInstanceState: Bundle?)

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(getLayoutID())
    onCreateActivity(savedInstanceState)
  }
  fun toolBar(view : androidx.appcompat.widget.Toolbar){
    view.setNavigationOnClickListener{
      finish()
    }
  }
  fun setImage(imageView: ImageView, urlString: String) {
    try {
      Picasso.with(this)
        .load(urlString)
        .into(imageView)
    } catch (e: Exception) {
      Toast.makeText(this, "Not load image", Toast.LENGTH_SHORT).show()
    }
  }
  fun setViewPaper(view : ViewPager,tabLayout : TabLayout,listFragment : ArrayList<Fragment>,default: Int?){
    view.adapter = adapterTabLayout(applicationContext,supportFragmentManager,tab_layout.getTabCount(),listFragment)
    view.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
    if(default != null){
      viewPager.currentItem = default
    }
    tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
      override fun onTabSelected(tab: TabLayout.Tab) {
        viewPager.currentItem = tab.position
      }
      override fun onTabUnselected(tab: TabLayout.Tab) {}
      override fun onTabReselected(tab: TabLayout.Tab) {}
    })
  }
  fun checkConnectNetwork() : Boolean {
    val connectivityManager = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
    return connectivityManager?.activeNetworkInfo?.isConnectedOrConnecting() ?: false
  }

  fun replaceFragment(container : Int,fragment: Fragment) {
    val transaction = supportFragmentManager
    transaction
      .beginTransaction()
      .replace(container, fragment)
      .commit()
  }
  fun runnable(handling : () -> Unit) {
    lateinit var runnable: Runnable
    runnable = Runnable {
      handling()
      Handler().postDelayed(runnable, 1000)
    }
    Handler().postDelayed(runnable, 1000)
  }
}