package com.nsolution.NPlayer.Base

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat.getSystemService
import com.nsolution.NPlayer.R
import com.nsolution.NPlayer.Home_Screen.Homescreen
import com.squareup.picasso.Picasso


abstract class BaseFragment : Fragment() {
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
  }
  fun toolBar(view : androidx.appcompat.widget.Toolbar){
    view.setNavigationIcon(R.drawable.icon_back_button)
    view.setNavigationOnClickListener{

    }
  }
  override fun onCreateView(
    inflater: LayoutInflater, container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    return provideYourFragmentView(inflater, container, savedInstanceState)
  }

  abstract fun provideYourFragmentView(
    inflater: LayoutInflater, container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View
  fun replaceFragment(fragment: Fragment) {
    val transaction = activity!!.supportFragmentManager
    transaction
      .beginTransaction()
      .replace(R.id.container, fragment)
      .addToBackStack(tag)
      .commit()
  }
  fun getImage(imageView: ImageView, urlString: String) {
    try {
      Picasso.with(context)
        .load(urlString)
        .into(imageView)
    } catch (e: Exception) {
      Toast.makeText(context, "Not load image", Toast.LENGTH_SHORT).show()
    }
  }
  fun ToastMessenger(text: String) {
    Toast.makeText(context, text, Toast.LENGTH_SHORT).show()
  }

  fun onChangeText(text: TextView, textError: TextView) {
    text.addTextChangedListener(object : TextWatcher {
      override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        textError.text = null
      }
      override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
      override fun afterTextChanged(s: Editable?) {}
    })
  }
  fun runnable(handling : () -> Unit) {
    lateinit var runnable: Runnable
    runnable = Runnable {
      handling()
      Handler().postDelayed(runnable, 1000)
    }
    Handler().postDelayed(runnable, 1000)
  }

  fun checkConnectNetwork(): Boolean {
    val connectivityManager =
      context!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
    return connectivityManager?.activeNetworkInfo?.isConnectedOrConnecting() ?: false
  }
}