package com.nsolution.NPlayer.Intro

import android.os.Bundle
import com.google.android.material.tabs.TabLayout
import com.nsolution.NPlayer.Base.BaseActivity
import com.nsolution.NPlayer.Data_Class.IntroItem
import com.nsolution.NPlayer.Navigation
import com.nsolution.NPlayer.R
import com.nsolution.NPlayer.Register.MainActivity
import kotlinx.android.synthetic.main.activity_user.*
import kotlinx.android.synthetic.main.activity_intro_screen.*
import kotlinx.android.synthetic.main.activity_intro_screen.tab_layout
import kotlinx.android.synthetic.main.activity_intro_screen.viewPager

class Intro_Screen : BaseActivity() {
  override fun getLayoutID(): Int {
    return R.layout.activity_intro_screen
  }
  val Item = ArrayList<IntroItem>()

  override fun onCreateActivity(savedInstanceState: Bundle?) {
    default()
    Item.add(IntroItem("Nguyễn", "", ""))
    Item.add(IntroItem("Quốc", "", ""))
    Item.add(IntroItem("Huy", "", ""))
    next_intro.setOnClickListener {
      var position = viewPager.currentItem
      if(position == Item.size - 1)
        lastItem()
      if(next_intro.text == "DONE" && position == Item.size - 1 )
        Navigation(this).gotoActivity(MainActivity::class.java, null, null,true)
      else {
        position++
        viewPager.currentItem = position
      }
    }
    skip_click.setOnClickListener {
      Navigation(this).gotoActivity(MainActivity::class.java, null, null,true)
    }

    viewPager.adapter = Intro_Adapter(this, Item)
    tab_layout.setupWithViewPager(viewPager)
    tab_layout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
      override fun onTabSelected(tab: TabLayout.Tab) {
        if (tab.position == Item.size - 1){
          lastItem()
        } else default()
      }
      override fun onTabUnselected(tab: TabLayout.Tab) {}
      override fun onTabReselected(tab: TabLayout.Tab) {}
    })
  }

  private fun lastItem(){
    next_intro.text = "DONE"
  }

  private fun default(){
    next_intro.text = "NEXT"
  }
}
