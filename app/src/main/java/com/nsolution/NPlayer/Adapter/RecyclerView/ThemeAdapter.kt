package com.nsolution.NPlayer.Adapter.RecyclerView

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nsolution.NPlayer.Models.ThemeDto

class ThemeAdapter(val list : ArrayList<ThemeDto>,val layout : Int) : BaseAdapter() {

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
    context = parent.context
    val view = LayoutInflater.from(context).inflate(layout, parent, false)
    return ThemeViewHolder(view)
  }

  override fun getItemCount(): Int = list.size

  fun onClick() {

  }

  override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
    val item = list[position]
    val viewHolder: ThemeViewHolder = holder as ThemeViewHolder
    viewHolder.title.text = item.title.toString()
    setImage(viewHolder.image,item.image.toString())
  }
}