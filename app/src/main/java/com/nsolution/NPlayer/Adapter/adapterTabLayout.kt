package com.nsolution.NPlayer.Adapter

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter


class adapterTabLayout(val context: Context,val fragment: FragmentManager?,val totalTabs: Int,val listFragment : ArrayList<Fragment>) : FragmentPagerAdapter(fragment!!) {

  override fun getCount(): Int {
    return totalTabs
  }
  override fun getItem(position: Int): Fragment {
    return listFragment[position]
  }
}
