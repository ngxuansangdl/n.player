package com.nsolution.NPlayer.Adapter.RecyclerView

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

abstract class BaseAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>(){

  var context : Context? = null

  fun setImage(imageView: ImageView, urlString: String?) {
    if (urlString != null && context != null) {
      try {
        Picasso.with(context)
          .load(urlString)
          .into(imageView)
      } catch (e: Exception) {
        Toast.makeText(context, "Not load image", Toast.LENGTH_SHORT).show()
      }
    }
  }
}