package com.nsolution.NPlayer.Adapter.RecyclerView

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nsolution.NPlayer.Models.CommentDto

class CommentAdapter(val list : ArrayList<CommentDto>,val layout : Int) : BaseAdapter() {
  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
    context = parent.context
    val view = LayoutInflater.from(context).inflate(layout, parent, false)
    return CommentViewHolder(view)
  }

  override fun getItemCount(): Int = list.size

  fun onClick() {

  }

  override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
    val item = list[position]
    val viewHolder: CommentViewHolder = holder as CommentViewHolder
    viewHolder.nameUser.text = item.name
    setImage(viewHolder.avatarUser, item.avatar)
    viewHolder.time.text = item.time
    viewHolder.description.text = item.context
    val bundle: Bundle = Bundle()
    viewHolder.avatarUser.apply {
      setOnClickListener {
        //thumbnailClick(User::class.java, bundle)
      }
    }
  }
}