package com.nsolution.NPlayer.Adapter.RecyclerView

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nsolution.NPlayer.Data_Class.layout
import com.nsolution.NPlayer.Interface_Service.Category
import com.nsolution.NPlayer.R

class List_Category(val list: ArrayList<Category>) :
  RecyclerView.Adapter<List_Category.ViewHolder>() {

  val layout = layout()

  override fun onBindViewHolder(holder: ViewHolder, position: Int) {

    val category = list[position]
    val typeList = category.type
    holder.title.text = category.title
    holder.recycler.apply {
      layoutManager = LinearLayoutManager(holder.recycler.context, RecyclerView.HORIZONTAL, false)
      when (typeList) {
        "artist" -> {
          val listThumbnail = category.listArtist!!
          adapter = ArtistAdapter(listThumbnail, artistView)
        }
        "notification" -> {
          val listThumbnail = category.listNotification!!
          adapter = NotificationAdpater(listThumbnail,notificationView)
        }
        "theme" -> {
          val listThumbnail = category.listTheme!!
          layoutManager = GridLayoutManager(context, 2)
          adapter = ThemeAdapter(listThumbnail, themeView)
        }
        "song" -> {
          val listThumbnail = category.listSong!!
          adapter = SongAdapter(listThumbnail, songView)
        }
        "vertical_list_video" -> {
          val listThumbnail = category.listVideo!!
          adapter = VideoAdapter(listThumbnail, verticalListVideoView)
        }
        "horizonal_list_video" -> {
          val listThumbnail = category.listVideo!!
          adapter = VideoAdapter(listThumbnail,horizonalListVideoView)
        }
        "playlist_horizonal" -> {
          val listThumbnail = category.listPlaylist!!
//          Log.d("playlist_vertical", ))
          adapter = PlaylistAdapter(listThumbnail, playlistView)
        }
        "playlist_vertical" -> {

          val listThumbnail = category.listPlaylist!!
          adapter = PlaylistAdapter(listThumbnail, playlistView)
        }
        "keyword" -> {
          val listThumbnail = category.listKeyword!!
          adapter = KeywordAdapter(listThumbnail, keywordView)
        }
        "comment" -> {
          val listThumbnail = category.listComment!!
          adapter = CommentAdapter(listThumbnail, commentView)
        }
      }
    }
  }


  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
    return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.thumbnail_list, parent, false))
  }

  override fun getItemCount(): Int {
    return list.size
  }

  class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val title = itemView.findViewById<TextView>(R.id.title)
    val recycler = itemView.findViewById<RecyclerView>(R.id.recycler)
  }
  companion object {
    private const val artistView = R.layout.thumbnail_artist
    private const val notificationView = R.layout.thumbnail_notification
    private const val themeView = R.layout.thumbnail_theme
    private const val songView = R.layout.thumbnail_song
    private const val horizonalListVideoView = R.layout.horizonal_list_video
    private const val verticalListVideoView = R.layout.vertical_list_video
    private const val playlistView = R.layout.thumbnail_playlist
    private const val keywordView = R.layout.thumbnail_keyword
    private const val commentView = R.layout.thumbnail_comment
  }
}

