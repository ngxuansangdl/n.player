package com.nsolution.NPlayer.Adapter.RecyclerView

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nsolution.NPlayer.Models.ArtistDto
import com.nsolution.NPlayer.User.User

class ArtistAdapter(val list : ArrayList<ArtistDto>,val layout : Int) : BaseAdapter() {
  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
    context = parent.context
    val view = LayoutInflater.from(context).inflate(layout, parent, false)
    return ArtistViewHolder(view)
  }

  override fun getItemCount(): Int = list.size

  fun onClick() {
    val intent : Intent = Intent(context, User::class.java)
    context!!.startActivity(intent)
  }

  override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
    val item = list[position]
    val viewHolder: ArtistViewHolder = holder as ArtistViewHolder
    viewHolder.artist.text = item.artists
    setImage(viewHolder.avatar, item.avatar)
    val bundle: Bundle = Bundle()
    viewHolder.thumbnail_click.setOnClickListener {
      onClick()
    }
  }
}