package com.nsolution.NPlayer.Adapter.RecyclerView

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nsolution.NPlayer.Media.Mp3.Mp3
import com.nsolution.NPlayer.MediaService
import com.nsolution.NPlayer.Models.SongDto

class SongAdapter(val list : ArrayList<SongDto>,val layout : Int) : BaseAdapter() {

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
    context = parent.context
    val view = LayoutInflater.from(context).inflate(layout, parent, false)
    return SongViewHolder(view)
  }

  override fun getItemCount(): Int = list.size
  private var mediaService: MediaService? = null
  private var playIntent: Intent? = null
  private var musicBound = false

  fun onClick() {
    if (playIntent == null) {
      playIntent = Intent(context, MediaService::class.java)
      context!!.bindService(playIntent, musicConnection, Context.BIND_AUTO_CREATE)
      context!!.startService(playIntent)
    } else {
      mediaService!!.setUri = "https://c1-ex-swe.nixcdn.com/Sony_Audio57/Havana-CamilaCabelloYoungThug-5817730.mp3?st=TYaZ2Jztm0ZLQ_SDiFfs4w&e=1587480535&t=1587394135857"
      mediaService!!.playSong()
    }
    //val intent : Intent = Intent(context,Mp3::class.java)
    //context!!.startActivity(intent)
  }

  override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
    val item = list[position]
    val viewHolder: SongViewHolder = holder as SongViewHolder
    viewHolder.artist.text = item.artists
    setImage(viewHolder.image, item.image)
    val bundle: Bundle = Bundle()
    viewHolder.thumbnail_click.setOnClickListener {
      onClick()
    }
  }


  private val musicConnection: ServiceConnection = object : ServiceConnection {
    override fun onServiceConnected(name: ComponentName, service: IBinder) {
      val binder = service as MediaService.MusicBinder
      mediaService = binder.getService()
      mediaService!!.setList = list
      mediaService!!.setUri = "https://c1-ex-swe.nixcdn.com/Sony_Audio57/Havana-CamilaCabelloYoungThug-5817730.mp3?st=TYaZ2Jztm0ZLQ_SDiFfs4w&e=1587480535&t=1587394135857"
      musicBound = true
    }
    override fun onServiceDisconnected(name: ComponentName) {
      musicBound = false
    }
  }
}