package com.nsolution.NPlayer.Adapter.RecyclerView

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nsolution.NPlayer.Media.Playlist
import com.nsolution.NPlayer.Models.PlaylistDto

class PlaylistAdapter(val list : ArrayList<PlaylistDto>,val layout : Int) : BaseAdapter() {
  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

    context = parent.context
    val view = LayoutInflater.from(context).inflate(layout, parent, false)
    return PlaylistViewHolder(view)
  }

  override fun getItemCount(): Int = list.size

  fun onClick() {
    val intent : Intent = Intent(context,Playlist::class.java)
    context!!.startActivity(intent)
  }

  override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
    val item = list[position]
    val viewHolder: PlaylistViewHolder = holder as PlaylistViewHolder
//    viewHolder.image.layoutParams.width
//    viewHolder.image.layoutParams.height = 200
    viewHolder.artist.text = item.artists
    viewHolder.title.text = item.title
    setImage(viewHolder.image, item.image)
    val bundle: Bundle = Bundle()
    viewHolder.thumbnail_click.setOnClickListener {
      onClick()
    }
  }
}