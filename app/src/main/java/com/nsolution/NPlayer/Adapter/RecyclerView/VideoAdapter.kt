package com.nsolution.NPlayer.Adapter.RecyclerView

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nsolution.NPlayer.Media.Mp4.Mp4
import com.nsolution.NPlayer.Models.VideoDto

class VideoAdapter(val list : ArrayList<VideoDto>,val layout : Int) : BaseAdapter() {
  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
    context = parent.context
    val view = LayoutInflater.from(context).inflate(layout, parent, false)
    return VideoViewHolder(view)
  }
  override fun getItemCount(): Int = list.size

  fun onClick() {
    val intent : Intent = Intent(context, Mp4::class.java)
    context!!.startActivity(intent)
  }
  override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
    val item = list[position]
    val viewHolder: VideoViewHolder = holder as VideoViewHolder
    viewHolder.title.text = item.title
    viewHolder.artist.text = item.artists
    setImage(viewHolder.image, item.image)
    if(item.live!!){
      viewHolder.live.visibility = View.VISIBLE
    } else viewHolder.live.visibility = View.INVISIBLE
    val bundle: Bundle = Bundle()
    viewHolder.thumbnail_click.setOnClickListener {
      onClick()
    }
  }
}