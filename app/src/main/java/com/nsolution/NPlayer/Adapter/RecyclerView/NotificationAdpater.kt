package com.nsolution.NPlayer.Adapter.RecyclerView

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nsolution.NPlayer.Models.NotificationDto

class NotificationAdpater(val list : ArrayList<NotificationDto>,val layout : Int) : BaseAdapter() {
  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
    context = parent.context
    val view = LayoutInflater.from(context).inflate(layout, parent, false)
    return NotificationViewHolder(view)
  }

  override fun getItemCount(): Int = list.size

  fun onClick() {
    //val intent : Intent = Intent(context,Mp3::class.java)
   // context!!.startActivity(intent)
  }

  override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
    val item = list[position]
    val viewHolder: NotificationViewHolder = holder as NotificationViewHolder
    viewHolder.nameUser.text = item.artists
    setImage(viewHolder.avatarUser, item.image)
    val bundle: Bundle = Bundle()
  }
}