package com.nsolution.NPlayer.Adapter.RecyclerView

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.nsolution.NPlayer.R


open class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {
  fun bindView(position: Int) {

  }
}

class PlaylistViewHolder(itemView: View) : ViewHolder(itemView){
  val title = itemView.findViewById<TextView>(R.id.title)
  val artist = itemView.findViewById<TextView>(R.id.artist)
  val image = itemView.findViewById<ImageView>(R.id.image)
  val thumbnail_click = itemView.findViewById<View>(R.id.thumbnail_click)
}

class VideoViewHolder(itemView: View) : ViewHolder(itemView){
  val title = itemView.findViewById<TextView>(R.id.title)
  val artist = itemView.findViewById<TextView>(R.id.artist)
  val image = itemView.findViewById<ImageView>(R.id.image)
  val live = itemView.findViewById<TextView>(R.id.live)
  val thumbnail_click = itemView.findViewById<View>(R.id.thumbnail_click)
}

class ArtistViewHolder(itemView: View) : ViewHolder(itemView){
  val artist = itemView.findViewById<TextView>(R.id.artist)
  val avatar = itemView.findViewById<ImageView>(R.id.avatar)
  val thumbnail_click = itemView.findViewById<View>(R.id.thumbnail_click)
}

class ThemeViewHolder(itemView : View) : ViewHolder(itemView){
  val title = itemView.findViewById<TextView>(R.id.title)
  val image = itemView.findViewById<ImageView>(R.id.image)
}

class SongViewHolder(itemView: View) : ViewHolder(itemView){
  val title = itemView.findViewById<TextView>(R.id.title)
  val artist = itemView.findViewById<TextView>(R.id.artist)
  val image = itemView.findViewById<ImageView>(R.id.image)
  val thumbnail_click = itemView.findViewById<View>(R.id.thumbnail_click)
}

class NotificationViewHolder(itemView: View) : ViewHolder(itemView){
  val nameUser = itemView.findViewById<TextView>(R.id.name_user)
  val avatarUser = itemView.findViewById<ImageView>(R.id.avatar)
  val contextDescription = itemView.findViewById<TextView>(R.id.description)
}

class CommentViewHolder(itemView: View) : ViewHolder(itemView){
  val avatarUser = itemView.findViewById<ImageView>(R.id.avatar_click)
  val nameUser = itemView.findViewById<TextView>(R.id.name)
  val time = itemView.findViewById<TextView>(R.id.time)
  val description = itemView.findViewById<TextView>(R.id.description)
}

class KeywordViewHolder(itemView: View) : ViewHolder(itemView){
  val keyword = itemView.findViewById<TextView>(R.id.keyword)
}
