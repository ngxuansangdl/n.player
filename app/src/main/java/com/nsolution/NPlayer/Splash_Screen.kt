package com.nsolution.NPlayer

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import com.nsolution.NPlayer.Get_Data_API.HttpClient
import com.nsolution.NPlayer.Home_Screen.Homescreen
import com.nsolution.NPlayer.Interface_Service.SignInInputDto
import com.nsolution.NPlayer.Intro.Intro_Screen
import com.nsolution.NPlayer.Register.MainActivity
import retrofit2.Call
import retrofit2.Response
import com.nsolution.NPlayer.Interface_Service.AccountResult
import com.nsolution.NPlayer.Interface_Service.IAccountServiceAPI
import com.nsolution.NPlayer.Interface_Service.Result

class Splash_Screen : AppCompatActivity() {
  val activity = this
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.splash_screen)
    Handler().postDelayed(Run, 3000)
  }

  private val Run = object : Runnable {

    override fun run() {
      val token = PreferencesAuthToken().getPrefData()
      val intro = PreferencesIntro().getPrefData()!!
      if (token != null) {
        //SignInWithToken(token!!).onfetch() --> WAIT API
        Navigation(activity).gotoActivity(Homescreen::class.java,null,null,true)
      } else if (intro && token == null) {
        Navigation(activity).gotoActivity(MainActivity::class.java, null,null, true)
      } else if (!intro && token == null){
        Navigation(activity).gotoActivity(Intro_Screen::class.java, null, null,true)
        PreferencesIntro().setPrefData(true)
      }
      return
    }
  }

  private fun PreferencesIntro() = object : PrefData<Boolean>(applicationContext, "myPrefs") {
    override fun getPrefData(): Boolean? {
      return sharedPreferences.getBoolean("haveIntroducedYet", false)
    }
    override fun setPrefData(data : Boolean?) {
      editor.putBoolean("haveIntroducedYet", data!!).commit()
    }
  }

  private fun PreferencesAuthToken() = object : PrefData<String>(applicationContext, "Authorization") {
    override fun getPrefData(): String? {
      return sharedPreferences.getString("haveAuthToken",null)
    }
    override fun setPrefData(data : String?) {
      editor.putString("haveAuthToken", data!!).commit()
    }
  }

  private fun SignInWithToken(token : String) =
    object : HttpClient<Result<AccountResult>>() {
      override fun callSync(): Call<Result<AccountResult>> {
        val service: IAccountServiceAPI = retrofit().create(IAccountServiceAPI::class.java)
        return service.signInWithToken(token)
      }
      override fun handling(response: Response<Result<AccountResult>>) {
        Log.d("tokenresult",response.toString())
        if(response.isSuccessful){
          val authToken = response.body()!!.result!!.authToken
          PreferencesAuthToken().setPrefData(authToken!!)
          Navigation(activity).gotoActivity(Homescreen::class.java,null,null,true)
        }
      }
    }
}
