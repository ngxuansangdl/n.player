package com.nsolution.NPlayer

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle

open class PrefData<T>(applicationContext: Context, namePrefs : String){ //myPrefs //haveIntroducedYet
  val sharedPreferences = applicationContext.getSharedPreferences(namePrefs, Context.MODE_PRIVATE)
  val editor : SharedPreferences.Editor = sharedPreferences.edit()
  open fun getPrefData() : T? = null
  open fun setPrefData(data : T?) {
    return
  }
  fun removePrefData(key : String){
    editor.remove(key)
  }
}
