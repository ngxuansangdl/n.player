package com.nsolution.NPlayer.Post

import android.graphics.Camera
import android.media.MediaPlayer
import android.os.Bundle
import android.view.SurfaceHolder
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nsolution.NPlayer.Base.BaseActivity
import com.nsolution.NPlayer.R
import kotlinx.android.synthetic.main.activity_start_live.*


class Start_LiveStream : BaseActivity() {


  private val mediaPlayer = MediaPlayer()
  private var surfaceHolder: SurfaceHolder? = null
  private val camera = Camera()
  private val activity = this

  private var comment : ArrayList<Any> = arrayListOf()

  override fun getLayoutID(): Int =
    R.layout.activity_start_live

  override fun onCreateActivity(savedInstanceState: Bundle?) {

    toolBar(toolbar)
    prepareLiveStream()

    start_live_click.setOnClickListener {
      startLiveStream()
    }
    finish_click.setOnClickListener {
      finishLiveStream()
    }
  }


  private fun prepareLiveStream(){
    list_comment.visibility = View.GONE
    controller.visibility = View.GONE
    live_status.visibility = View.GONE
  }

  private fun startLiveStream(){
    list_comment.visibility = View.VISIBLE
    controller.visibility = View.VISIBLE
    live_status.visibility = View.VISIBLE
    write_layout.visibility = View.GONE
    action_bar.visibility = View.GONE
    start_live_click.visibility = View.GONE
  }

//  private fun addComment(item : CommentThumbnail){
//    comment.add(item)
//
//  }
//  private fun listComment(){
//    list_comment.setHasFixedSize(true)
//    list_comment.layoutManager  = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
//    list_comment.adapter =
//  }
  private fun finishLiveStream(){
    finish()
  }

}
