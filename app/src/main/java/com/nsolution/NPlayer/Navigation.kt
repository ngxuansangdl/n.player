package com.nsolution.NPlayer

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.core.app.ShareCompat.getCallingActivity
import com.nsolution.NPlayer.Register.MainActivity


class Navigation(val currentActivity : Activity) {
  fun gotoActivity(activity : Class<*>, bundle : Bundle?,flags : Int?,close : Boolean){
    val intent = Intent(currentActivity,activity)
    if(bundle != null)
      intent.putExtras(bundle)
    if(flags == null)
      intent.flags = 0
    else intent.flags = flags
    currentActivity.startActivity(intent)
    if(close)
      currentActivity.finish()
  }
}