package com.nsolution.NPlayer.Data_Class

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.nsolution.NPlayer.R

class layout {
  val HORIZONAL_TYPE = "horizonal"
  val VERTICAL_TYPE = "vertical"
  val GIRD_TYPE = "grid"
}



class Constant {
  val ItemSong = 0
  val ItemArtist = 1
}

data class IntroItem(
  val title: String,
  val subtitle: String,
  val image: String
)


