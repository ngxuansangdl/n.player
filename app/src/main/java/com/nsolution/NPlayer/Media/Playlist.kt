package com.nsolution.NPlayer.Media

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.nsolution.NPlayer.Adapter.RecyclerView.SongAdapter
import com.nsolution.NPlayer.Base.BaseActivity
import com.nsolution.NPlayer.Media.Mp3.Mp3
import com.nsolution.NPlayer.MediaService
import com.nsolution.NPlayer.MediaService.MusicBinder
import com.nsolution.NPlayer.Models.SongDto
import com.nsolution.NPlayer.Navigation
import com.nsolution.NPlayer.R
import kotlinx.android.synthetic.main.activity_album.*


class Playlist : BaseActivity() {

  private val TITLE = "TITLE"
  private val ARTIST = "ARTIST"
  private val IMAGE = "IMAGE"
  val list : ArrayList<SongDto> = arrayListOf()

  override fun getLayoutID(): Int = R.layout.activity_album

  override fun onCreateActivity(savedInstanceState: Bundle?) {
    toolBar(toolbar)
    getData()
    runnable { showHide() }
    play_all.setOnClickListener {
      val bundle = Bundle()
      playAllClick(bundle)
    }

    val item : SongDto = SongDto()
    item.title = "Havana"
    item.artists = "Camila Cabello"
    item.image = "https://i.scdn.co/image/ab67616d0000b273d93cf4d07ba50d7b32ca7c44"

    list.add(item)
    list.add(item)
    list.add(item)
    list.add(item)
    list.add(item)

    recycler.layoutManager = LinearLayoutManager(this)
    recycler.adapter = SongAdapter(list!!, R.layout.thumbnail_song)
  }

  private fun showHide(){
    val bundle = intent.extras
    val check = titleItem.visibility == View.GONE
    if (check)
      toolbar.title = bundle?.getString(TITLE)
  }

  private fun playAllClick(bundle: Bundle?){
    Navigation(this).gotoActivity(Mp3::class.java,bundle,null,false)
  }



  private fun getData(){
    val bundle = intent.extras
    titleItem.text = bundle?.getString(TITLE)
    artistItem.text = bundle?.getString(ARTIST)
    val Image = bundle?.getString(IMAGE)
    setImage(imageItem,Image.toString())
  }

}
