package com.nsolution.NPlayer.Media.Mp4

import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.nsolution.NPlayer.Base.BaseActivity
import com.nsolution.NPlayer.Navigation
import com.nsolution.NPlayer.R
import com.nsolution.NPlayer.User.User
import kotlinx.android.synthetic.main.activity_live_view.*
import kotlinx.android.synthetic.main.activity_live_view.controller
import kotlinx.android.synthetic.main.activity_mp4.artistItem
import kotlinx.android.synthetic.main.activity_mp4.loading
import kotlinx.android.synthetic.main.activity_mp4.play_mv
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class Live_View : BaseActivity() {


  private val TITLE = "TITLE"
  private val ARTIST = "ARTIST"
  private val NAME = "NAME"
  private val AVATAR = "AVATAR"
  var Image = ""

  var artists : String? = null


  override fun getLayoutID(): Int {
    return R.layout.activity_live_view
  }
  override fun onCreateActivity(savedInstanceState: Bundle?) {
    val bundle = intent.extras
    artists = bundle?.getString(ARTIST)
    artistItem.text = artists
    getData()
    avatar_click.setOnClickListener {
      val bundle1: Bundle = Bundle()
      bundle1.apply {
        putString(NAME,artists)
        putString(AVATAR,Image)
      }
      Navigation(this).gotoActivity(User::class.java,bundle1,null,false)
    }
    runnable { showHideLoading() }
  }
  private fun showHideLoading(){
    if (play_mv.isPlaying){
      loading.visibility = View.GONE
    }
  }

  private fun getData(){
    val httpClient : OkHttpClient.Builder = OkHttpClient.Builder()
    val retrofit : Retrofit = Retrofit.Builder()
      .baseUrl("http://192.168.1.103:3000/")
      .addConverterFactory(GsonConverterFactory.create())
      .client(httpClient.build())
      .build()
  }
  private fun playVideo(url : String){
    loading.visibility = View.VISIBLE
    try {
      play_mv.setVideoPath(url)
      play_mv.start()
    } catch (e : Exception){
      Toast.makeText(this,"No load video",Toast.LENGTH_SHORT).show()
    }
  }
}
