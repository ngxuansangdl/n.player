package com.nsolution.NPlayer.Media.Mp3

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nsolution.NPlayer.Base.BaseFragment

import com.nsolution.NPlayer.R
import kotlinx.android.synthetic.main.fragment_main_paper.*

class Main_Paper : BaseFragment() {

  override fun provideYourFragmentView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View {
    return inflater.inflate(R.layout.fragment_main_paper, container, false)
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    artistItem.text = "Lana Del rey"
    titleItem.text = "Born To Die"
    getImage(imageItem,"https://resources.wimpmusic.com/images/2a0d3b1c/0c57/4d04/8fd0/488fa3ca3d48/640x640.jpg")
  }
}
