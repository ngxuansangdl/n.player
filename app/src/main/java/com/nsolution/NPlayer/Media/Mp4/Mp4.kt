package com.nsolution.NPlayer.Media.Mp4

import android.media.MediaPlayer.OnPreparedListener
import android.os.Bundle
import android.view.View
import android.view.View.OnTouchListener
import android.widget.SeekBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.nsolution.NPlayer.Base.BaseActivity
import com.nsolution.NPlayer.R
import kotlinx.android.synthetic.main.activity_mp4.*
import android.os.Handler
import kotlinx.android.synthetic.main.activity_mp3.*
import kotlinx.android.synthetic.main.activity_mp4.media_controller
import kotlinx.android.synthetic.main.activity_mp4.seekBar
import kotlinx.android.synthetic.main.activity_mp4.tab_layout
import kotlinx.android.synthetic.main.activity_mp4.viewPager


class Mp4 : BaseActivity() {

  private val TITLE = "TITLE"
  private val ARTIST = "ARTIST"

  private val DefaultPosition = 0
  override fun getLayoutID(): Int {
    return  R.layout.activity_mp4
  }

  override fun onCreateActivity(savedInstanceState: Bundle?) {
    setContentView(R.layout.activity_mp4)
    val bundle = intent.extras
    val Title = bundle?.getString(TITLE)
    val Artist = bundle?.getString(ARTIST)
    setPaper()
    seekBar.setPadding(0,0,0,0)
    titleItem.text = Title
    artistItem.text = Artist
    mediaController()
    runnable { showHideLoading() }
  }
  private fun setPaper(){
    val listFragment : ArrayList<Fragment> = arrayListOf()
    listFragment.add(Next_Video())
    listFragment.add(Comment_Video())
    listFragment.add(Detail_Video())
    setViewPaper(viewPager,tab_layout,listFragment,DefaultPosition)
  }
  private fun showHideLoading(){
    if (play_mv.isPlaying){
      loading.visibility = View.GONE
    }
  }

  private fun showMediaController(){
    media_controller.visibility = View.VISIBLE
  }
  private fun hideMediaController(){
    if(media_controller.visibility == View.VISIBLE)
      Handler().postDelayed({
      media_controller.visibility = View.INVISIBLE
    },3000)
    else media_controller.visibility = View.INVISIBLE
  }

  private fun mediaController(){
    play_mv.setOnTouchListener(OnTouchListener { view, event ->
      showMediaController()
      hideMediaController()
      true
    })
    play_pause.setOnClickListener {
      showMediaController()
      if(play_mv.isPlaying && play_mv != null){
        play_mv.pause()
        play_pause.setImageResource(R.drawable.icon_pause_arrow)
        loading.visibility = View.GONE
      } else {
        play_mv.start()
        play_pause.setImageResource(R.drawable.icon_play_arrow)
      }
    }
    seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
      override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
        if (fromUser)
          showMediaController()
          play_mv.seekTo(progress)
      }
      override fun onStartTrackingTouch(seekBar: SeekBar) {}
      override fun onStopTrackingTouch(seekBar: SeekBar) {}
    })

  }

  override fun onStart() {
    super.onStart()
    loading.visibility = View.VISIBLE
    try {
      //play_mv.setVideoPath("https://tpcom.live1.vn/hls-source/5a96d716-665c-4143-91a9-c3dc428070a6/2638d1a8-3c65-4087-b4bc-938691cf7cc9/playlist.m3u8")
      play_mv.setVideoPath("https://vnso-zn-15-tf-mcloud-mpl-s7-mv-zmp3.zadn.vn//vnso-zn-15-tf-mcloud-lpl-s7-mv-zmp3.zadn.vn/CMCSTF3B88k/whls/vod/480/dPYPdsJKrTNrzVRMy4a/Nevada.m3u8?authen=exp=1586513915~acl=/CMCSTF3B88k/*~hmac=ce96d3f12cd2c9b7d282e38e42906480")
      play_mv.setOnPreparedListener(OnPreparedListener {
        seekBar.max = play_mv.duration
      })
      play_mv.resume()
      runnable { Progress() }
      play_mv.start()
    } catch (e : Exception){
      Toast.makeText(this,"No load video",Toast.LENGTH_SHORT).show()
    }
  }
  private fun Progress(){
    seekBar.progress = play_mv.currentPosition
  }
}
