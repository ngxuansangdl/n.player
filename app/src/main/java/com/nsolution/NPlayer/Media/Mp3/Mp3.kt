package com.nsolution.NPlayer.Media.Mp3

import android.media.AudioManager
import android.media.MediaPlayer
import android.os.Bundle
import android.widget.SeekBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.nsolution.NPlayer.Base.BaseActivity
import com.nsolution.NPlayer.R
import kotlinx.android.synthetic.main.activity_mp3.*
import kotlinx.android.synthetic.main.activity_mp3.seekBar
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class Mp3 : BaseActivity() {


  private val DefaultPosition = 1
  val mediaPlayer: MediaPlayer = MediaPlayer()

  override fun getLayoutID(): Int = R.layout.activity_mp3

  override fun onCreateActivity(savedInstanceState: Bundle?) {
    toolBar(toolbar)
    val listPaper : ArrayList<Fragment> = arrayListOf()
    listPaper.add(Detail_Paper())
    listPaper.add(Main_Paper())
    listPaper.add(Lyrics_Paper())
    setViewPaper(viewPager,tab_layout,listPaper,DefaultPosition)
    getSetData()
    //val uri = "https://c1-ex-swe.nixcdn.com/Sony_Audio57/Havana-CamilaCabelloYoungThug-5817730.mp3?st=TYaZ2Jztm0ZLQ_SDiFfs4w&e=1587480535&t=1587394135857"
    //PlayMusic(uri)
    MediaController()
    seekBar.setPadding(0,0,0,0)
  }

  private fun getSetData() {
    val httpClient : OkHttpClient.Builder = OkHttpClient.Builder()
    val retrofit : Retrofit = Retrofit.Builder()
      .baseUrl("http://192.168.1.103:3000/")
      .addConverterFactory(GsonConverterFactory.create())
      .client(httpClient.build())
      .build()
  }

  private fun MediaController() {
    pause_start.apply {
      setOnClickListener {
        if (mediaPlayer.isPlaying) {
          setImageResource(R.drawable.icon_pause_arrow)
          mediaPlayer.pause()
        } else {
          setImageResource(R.drawable.icon_play_media)
          mediaPlayer.start()
        }
      }
    }
    favourite_click.apply {
      setOnClickListener {
        if (it.tag == R.drawable.icon_favourite) {
          setImageResource(R.drawable.icon_un_favourite)
          Toast.makeText(context, "Removed from favorites", Toast.LENGTH_SHORT).show()
        } else {
          setImageResource(R.drawable.icon_favourite)
          Toast.makeText(context, "Added to favorites", Toast.LENGTH_SHORT).show()
        }
      }
    }
    seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
      override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
        if (fromUser)
          mediaPlayer.seekTo(progress)
      }
      override fun onStartTrackingTouch(seekBar: SeekBar) {}
      override fun onStopTrackingTouch(seekBar: SeekBar) {}
    })
  }

  private fun PlayMusic(uri: String) {
    mediaPlayer.reset()
    try {
      mediaPlayer.apply {
        setAudioStreamType(AudioManager.STREAM_MUSIC)
        setDataSource(uri)
        prepare()
        runnable { Progress() }
        seekBar.max = duration
        end.text = Time(duration)
        start()
      }
    } catch (e: java.lang.Exception) {
      Toast.makeText(this, "No load music", Toast.LENGTH_SHORT).show()
    }
  }

  private fun Time(time: Int): String {
    return "${(time / 1000) / 60}:${(time / 1000) % 60}"
  }

  private fun Progress() {
    val current: Int = mediaPlayer.currentPosition
    seekBar.progress = current
    start.text = Time(current)
    if (current == mediaPlayer.duration - 1) {
      seekBar.progress = 0
      start.text = "0:00"
    }
  }
}
